export interface Pet {
    id: string;
    id_clinic: string;
    name: string;
    type: string;
    specie: string;
    size: number;
    weight: number;
    description: string;
    birthdate: string;
    inscriptiondate: string;
    state: string;
    ownerid: string
}
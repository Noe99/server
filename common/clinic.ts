export interface Clinic {
    id: string;
    name: string;
    adress: string;
    phonenb: string;
    faxnb: string;
    managerid: string;
    numberofemploye: number;
}
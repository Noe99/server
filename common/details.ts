export interface Details {
    id: string;
    description: string;
    price: number;
}
import * as express from "express";
import * as bodyParser from 'body-parser';
import * as cors from "cors";
import * as pg from "pg";
import { Clinic } from '../common/clinic'
import { Employe } from '../common/employe'
import { Pet } from '../common/pet'
import { Owner } from '../common/owner'
import { Occurence } from '../common/occurence'
import { ClinicSalary } from '../common/clinicSalary'
import { Traitement } from '../common/traitement'
import { Bill } from '../common/bill'
import { Details } from '../common/details'

const app = express();
const port: number = 5000;
const Pool = pg.Pool;
function loggerMiddleware(request: express.Request, response: express.Response, next: () => void) {
    console.log(`${request.method} ${request.path}`);
    next();
}
app.use(loggerMiddleware);
app.use(bodyParser.json());
app.use(cors());

const connectionConfig = new Pool({
    user: "doadmin",
    password: "ay3ojizvoz4g7lcz",
    host: "db-postgresql-tor1-03207-do-user-9071791-0.b.db.ondigitalocean.com",
    port: 25060,
    database: "defaultdb",
    ssl: {
        rejectUnauthorized: false,
    },
});
//

/***********ROUTER ***********/
const router = express.Router();
const router2 = express.Router();
const router3 = express.Router();
const router4 = express.Router();
const router5 = express.Router();
const router6 = express.Router();
const router7 = express.Router();
const router8 = express.Router();
const router9 = express.Router();
app.use('/clinic', router);
app.use('/employe', router2);
app.use('/pet', router3);
app.use('/owner', router4);
app.use('/salary', router5);
app.use('/occurence', router6);
app.use('/traitement', router7);
app.use('/bill', router8);
app.use('/detail', router9);
/*****************************/


app.get('/', (req, res) => {
    res.send('you are connect ! second server');
})


/***************************************************CLINICs******************************************************/
router.post('/', async (req, res) => {
    console.log('server anylyse POST Request');
    let clinic: Clinic = req.body;
    const query = await connectionConfig.query(`INSERT INTO Clinic VALUES (
    '${clinic.id}', 
    '${clinic.name}', 
    '${clinic.adress}', 
    '${clinic.phonenb}', 
    '${clinic.faxnb}',
    '${clinic.managerid}',
    '${clinic.numberofemploye}'
    );`).catch((err) => {
        console.log(err)
    });
    res.json(query);
})

router.get('/clinics', async (req, res) => {
    console.log('server anylyse GET Request');
    const clients = await connectionConfig.query("SELECT * FROM Clinic ORDER BY id;");
    let result: Clinic[] = clients.rows.map((clinic: Clinic) => ({
        id: clinic.id,
        name: clinic.name,
        adress: clinic.adress,
        phonenb: clinic.phonenb,
        faxnb: clinic.faxnb,
        managerid: clinic.managerid,
        numberofemploye: clinic.numberofemploye,
    }));
    res.json(result);
})


router.delete('/:id', async (req: express.Request, res: express.Response) => {
    console.log('server anylyse DELETE Request');
    let id: string = req.params.id;
    console.log(req.params.id);
    const query = await connectionConfig.query(`DELETE FROM Clinic WHERE id = '${id}';`).catch((err) => {
        console.log(err)
    });
    res.json(query);
})
/****************************************************************************************************************/



/***************************************************EMPLOYES******************************************************/

router2.get('/employes', async (req, res) => {
    console.log('server anylyse GET Request');
    const clients = await connectionConfig.query("SELECT * FROM Employe ORDER BY id;");
    let result: Employe[] = clients.rows.map((employe: Employe) => ({
        id: employe.id,
        name: employe.name,
        phonenb: employe.phonenb,
        birthdate: employe.birthdate,
        sexe: employe.sexe,
        nas: employe.nas,
        fonction: employe.fonction,
        salary: employe.salary,
    }));
    console.log(result);
    res.json(result);
})

router2.get('/employes40', async (req, res) => {
    console.log('server anylyse GET Request');
    const clients = await connectionConfig.query("SELECT * FROM Employe WHERE( (SELECT EXTRACT (YEAR from AGE(birthdate))AS AGE) > 40)ORDER BY name;");
    let result: Employe[] = clients.rows.map((employe: Employe) => ({
        id: employe.id,
        name: employe.name,
        phonenb: employe.phonenb,
        birthdate: employe.birthdate,
        sexe: employe.sexe,
        nas: employe.nas,
        fonction: employe.fonction,
        salary: employe.salary,
    }));
    console.log(result);
    res.json(result);
})

router2.post('/', async (req, res) => {
    console.log('server anylyse POST Request');
    let employe: Employe = req.body;
    const query = await connectionConfig.query(`INSERT INTO Employe VALUES (
        '${employe.id}', 
        '${employe.name}', 
        '${employe.phonenb}', 
        '${employe.birthdate}', 
        '${employe.sexe}',
        '${employe.nas}',
        '${employe.fonction}',
        '${employe.salary}'
        );`).catch((err) => {
        console.log(err)
    });
    res.json(query);
})

router2.delete('/:id', async (req: express.Request, res: express.Response) => {
    console.log('server anylyse DELETE Request');
    let id: string = req.params.id;
    console.log(req.params.id);
    const query = await connectionConfig.query(`DELETE FROM Employe WHERE id = '${id}';`).catch((err) => {
        console.log(err)
    });
    res.json(query);
})





/****************************************************************************************************************/

/****************************************************PETS********************************************************/
router3.post('/', async (req, res) => {
    console.log('server anylyse POST Request');
    let pet: Pet = req.body;
    console.log(pet);
    const query = await connectionConfig.query(`INSERT INTO Pet VALUES (
    '${pet.id}', 
    '${pet.id_clinic}', 
    '${pet.name}', 
    '${pet.type}', 
    '${pet.specie}',
    '${pet.size}',
    '${pet.weight}',
    '${pet.description}', 
    '${pet.birthdate}', 
    '${pet.inscriptiondate}',
    '${pet.state}',
    '${pet.ownerid}'
    );`).catch((err) => {
        console.log(err)
    });
    res.json(query);
})

router3.get('/pets', async (req, res) => {
    console.log('server anylyse GET Request');
    const clients = await connectionConfig.query("SELECT * FROM Pet ORDER BY id;");
    let result: Pet[] = clients.rows.map((pet: Pet) => ({
        id: pet.id,
        id_clinic: pet.id_clinic,
        name: pet.name,
        type: pet.type,
        specie: pet.specie,
        size: pet.size,
        weight: pet.weight,
        description: pet.description,
        state: pet.state,
        ownerid: pet.ownerid,
        birthdate: pet.birthdate,
        inscriptiondate: pet.inscriptiondate,
    }));
    //console.log(result);
    res.json(result);
})

router3.get('/:name', async (req, res) => {
    console.log('server anylyse GET Request');
    let name: string = req.params.name;
    const clients = await connectionConfig.query(`SELECT * FROM Pet WHERE name LIKE '%${name}%'ORDER BY id;`);
    let result: Pet[] = clients.rows.map((pet: Pet) => ({
        id: pet.id,
        id_clinic: pet.id_clinic,
        name: pet.name,
        type: pet.type,
        specie: pet.specie,
        size: pet.size,
        weight: pet.weight,
        description: pet.description,
        state: pet.state,
        ownerid: pet.ownerid,
        birthdate: pet.birthdate,
        inscriptiondate: pet.inscriptiondate,
    }));
    //console.log(result);
    res.json(result);
})

router3.get('/:id', async (req, res) => {
    console.log('server anylyse GET Request');
    let id: string = req.params.id;
    const clients = await connectionConfig.query(`SELECT * FROM Pet WHERE OwnerId = '${id}' ORDER BY id;`);
    let result: Pet[] = clients.rows.map((pet: Pet) => ({
        id: pet.id,
        id_clinic: pet.id_clinic,
        name: pet.name,
        type: pet.type,
        specie: pet.specie,
        size: pet.size,
        weight: pet.weight,
        description: pet.description,
        state: pet.state,
        ownerid: pet.ownerid,
        birthdate: pet.birthdate,
        inscriptiondate: pet.inscriptiondate,
    }));
    //console.log(result);
    res.json(result);
})

router3.delete('/:id', async (req: express.Request, res: express.Response) => {
    console.log('server anylyse DELETE Request');
    let id: string = req.params.id;
    console.log(req.params.id);
    const query = await connectionConfig.query(`DELETE FROM Pet WHERE id = '${id}';`).catch((err) => {
        console.log(err)
    });
    res.json(query);
})
/****************************************************************************************************************/

/****************************************************OWNERS********************************************************/
router4.post('/', async (req, res) => {
    console.log('server anylyse POST Request');
    let owner: Owner = req.body;
    console.log(owner);
    const query = await connectionConfig.query(`INSERT INTO Owner VALUES (
    '${owner.id}', 
    '${owner.id_clinic}', 
    '${owner.name}', 
    '${owner.adress}', 
    '${owner.phonenb}'
    );`).catch((err) => {
        console.log(err)
    });
    res.json(query);
})

router4.get('/owners', async (req, res) => {
    console.log('server anylyse GET Request');
    const clients = await connectionConfig.query("SELECT * FROM Owner ORDER BY id;");
    let result: Owner[] = clients.rows.map((owner: Owner) => ({
        id: owner.id,
        id_clinic: owner.id_clinic,
        name: owner.name,
        adress: owner.adress,
        phonenb: owner.phonenb,
    }));
    //console.log(result);
    res.json(result);
})

router4.get('/:id', async (req, res) => {
    console.log('server anylyse GET Request');
    let id: string = req.params.id;
    const clients = await connectionConfig.query(`SELECT * FROM Owner WHERE id_clinic = '${id}' ORDER BY id;`);
    let result: Owner[] = clients.rows.map((owner: Owner) => ({
        id: owner.id,
        id_clinic: owner.id_clinic,
        name: owner.name,
        adress: owner.adress,
        phonenb: owner.phonenb,
    }));
    //console.log(result);
    res.json(result);
})

router4.delete('/:id', async (req: express.Request, res: express.Response) => {
    console.log('server anylyse DELETE Request');
    let id: string = req.params.id;
    console.log(req.params.id);
    const query = await connectionConfig.query(`DELETE FROM Owner WHERE id = '${id}';`).catch((err) => {
        console.log(err)
    });
    res.json(query);
})
/****************************************************************************************************************/

/**************************************************SALARY****************************************************/
router5.get('/clinicsSalarys', async (req, res) => {
    console.log('server anylyse GET Request');
    let query: string = 'SELECT c.Id , SUM(e.salary) AS salary FROM clinic c LEFT JOIN Employe e ON ( e.id = (SELECT id_employe FROM ClinicEmploye WHERE id_employe = e.id AND c.id = id_clinic) ) GROUP BY c.Id;';
    const clients = await connectionConfig.query(query);
    let result: ClinicSalary[] = clients.rows.map((salary: ClinicSalary) => ({
        id: salary.id,
        salary: salary.salary,
    }));
    res.json(result);
})
/****************************************************************************************************************/

router6.get('/occurences', async (req, res) => {
    console.log('server anylyse GET Request');
    const clients = await connectionConfig.query("SELECT name, Count(*) as occurence FROM Pet GROUP BY name;");
    let result: Occurence[] = clients.rows.map((occurence: Occurence) => ({
        name: occurence.name,
        occurence: occurence.occurence,
    }));
    res.json(result);
})
/****************************************************************************************************************/

/****************************************************************************************************************/

router7.get('/traitements', async (req, res) => {
    console.log('server anylyse GET Request');
    const clients = await connectionConfig.query("SELECT p.ExamID, e.Date, e.petId, e.VeterinaryId, p.TraitementId, p.billid, p.quantity, p.StartDate , p.EndDate FROM Prescription p, Exam e WHERE e.id = p.ExamID;");
    let result: Traitement[] = clients.rows.map((traitement: Traitement) => ({
        examid: traitement.examid,
        date: traitement.date,
        petid: traitement.petid,
        veterinaryid: traitement.veterinaryid,
        traitementid: traitement.traitementid,
        billid: traitement.billid,
        quantity: traitement.quantity,
        startdate: traitement.startdate,
        enddate: traitement.enddate
    }));
    res.json(result);
})


router7.get('/:id', async (req, res) => {
    console.log('server anylyse GET Request');
    let id: string = req.params.id;
    const clients = await connectionConfig.query(`SELECT p.ExamID, e.Date, e.petId, e.VeterinaryId, p.TraitementId, p.billid, p.quantity, p.StartDate , p.EndDate FROM Prescription p, Exam e WHERE e.id = p.ExamID AND e.petId = '${id}' ;`);
    let result: Traitement[] = clients.rows.map((traitement: Traitement) => ({
        examid: traitement.examid,
        date: traitement.date,
        petid: traitement.petid,
        veterinaryid: traitement.veterinaryid,
        traitementid: traitement.traitementid,
        billid: traitement.billid,
        quantity: traitement.quantity,
        startdate: traitement.startdate,
        enddate: traitement.enddate
    }));
    res.json(result);
})
/****************************************************************************************************************/

/*************************************************BILL**********************************************************/
router8.get('/:id', async (req, res) => {
    console.log('server anylyse GET Request');
    let id: string = req.params.id;
    const clients = await connectionConfig.query(`SELECT * FROM Bill WHERE id = '${id}' ;`);
    let result: Bill[] = clients.rows.map((bill: Bill) => ({
        id: bill.id,
        ownerid: bill.ownerid,
        petid: bill.petid,
        veterinaryid: bill.veterinaryid,
        date: bill.date,
        paymentmethod: bill.paymentmethod,
        paymentaccepted: bill.paymentaccepted,
        total: bill.total,
    }));
    res.json(result);
})

router8.post('/', async (req, res) => {
    console.log('server anylyse POST Request');
    let bill: Bill = req.body;
    console.log(bill);
    const query = await connectionConfig.query(`INSERT INTO Bill VALUES (
    '${bill.id}', 
    '${bill.ownerid}', 
    '${bill.petid}', 
    '${bill.veterinaryid}',
    '${bill.date}', 
    '${bill.paymentmethod}',
    '${bill.paymentaccepted}', 
    '${bill.total}'
    );`).catch((err) => {
        console.log(err)
    });
    res.json(query);
})
/*************************************************BILL**********************************************************/

/*************************************************TREATEMENT DETAILS**********************************************************/
router9.get('/:id', async (req, res) => {
    console.log('server anylyse GET Request');
    let id: string = req.params.id;
    const clients = await connectionConfig.query(`SELECT * FROM Treatment WHERE id = '${id}' ;`);
    let result: Details[] = clients.rows.map((detail: Details) => ({
        id: detail.id,
        description: detail.description,
        price: detail.price,
    }));
    res.json(result);
})
/*************************************************TREATEMENT DETAILS**********************************************************/

app.listen(port, () => {
    console.log('serveur running on port ' + port);
})
"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const pg = require("pg");
const app = express();
const port = 5000;
const Pool = pg.Pool;
function loggerMiddleware(request, response, next) {
    console.log(`${request.method} ${request.path}`);
    next();
}
app.use(loggerMiddleware);
app.use(bodyParser.json());
app.use(cors());
const connectionConfig = new Pool({
    user: "doadmin",
    password: "ay3ojizvoz4g7lcz",
    host: "db-postgresql-tor1-03207-do-user-9071791-0.b.db.ondigitalocean.com",
    port: 25060,
    database: "defaultdb",
    ssl: {
        rejectUnauthorized: false,
    },
});
//
/***********ROUTER ***********/
const router = express.Router();
const router2 = express.Router();
const router3 = express.Router();
const router4 = express.Router();
const router5 = express.Router();
const router6 = express.Router();
const router7 = express.Router();
const router8 = express.Router();
const router9 = express.Router();
app.use('/clinic', router);
app.use('/employe', router2);
app.use('/pet', router3);
app.use('/owner', router4);
app.use('/salary', router5);
app.use('/occurence', router6);
app.use('/traitement', router7);
app.use('/bill', router8);
app.use('/detail', router9);
/*****************************/
app.get('/', (req, res) => {
    res.send('you are connect ! second server');
});
/***************************************************CLINICs******************************************************/
router.post('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse POST Request');
    let clinic = req.body;
    const query = yield connectionConfig.query(`INSERT INTO Clinic VALUES (
    '${clinic.id}', 
    '${clinic.name}', 
    '${clinic.adress}', 
    '${clinic.phonenb}', 
    '${clinic.faxnb}',
    '${clinic.managerid}',
    '${clinic.numberofemploye}'
    );`).catch((err) => {
        console.log(err);
    });
    res.json(query);
}));
router.get('/clinics', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    const clients = yield connectionConfig.query("SELECT * FROM Clinic ORDER BY id;");
    let result = clients.rows.map((clinic) => ({
        id: clinic.id,
        name: clinic.name,
        adress: clinic.adress,
        phonenb: clinic.phonenb,
        faxnb: clinic.faxnb,
        managerid: clinic.managerid,
        numberofemploye: clinic.numberofemploye,
    }));
    res.json(result);
}));
router.delete('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse DELETE Request');
    let id = req.params.id;
    console.log(req.params.id);
    const query = yield connectionConfig.query(`DELETE FROM Clinic WHERE id = '${id}';`).catch((err) => {
        console.log(err);
    });
    res.json(query);
}));
/****************************************************************************************************************/
/***************************************************EMPLOYES******************************************************/
router2.get('/employes', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    const clients = yield connectionConfig.query("SELECT * FROM Employe ORDER BY id;");
    let result = clients.rows.map((employe) => ({
        id: employe.id,
        name: employe.name,
        phonenb: employe.phonenb,
        birthdate: employe.birthdate,
        sexe: employe.sexe,
        nas: employe.nas,
        fonction: employe.fonction,
        salary: employe.salary,
    }));
    console.log(result);
    res.json(result);
}));
router2.get('/employes40', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    const clients = yield connectionConfig.query("SELECT * FROM Employe WHERE( (SELECT EXTRACT (YEAR from AGE(birthdate))AS AGE) > 40)ORDER BY name;");
    let result = clients.rows.map((employe) => ({
        id: employe.id,
        name: employe.name,
        phonenb: employe.phonenb,
        birthdate: employe.birthdate,
        sexe: employe.sexe,
        nas: employe.nas,
        fonction: employe.fonction,
        salary: employe.salary,
    }));
    console.log(result);
    res.json(result);
}));
router2.post('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse POST Request');
    let employe = req.body;
    const query = yield connectionConfig.query(`INSERT INTO Employe VALUES (
        '${employe.id}', 
        '${employe.name}', 
        '${employe.phonenb}', 
        '${employe.birthdate}', 
        '${employe.sexe}',
        '${employe.nas}',
        '${employe.fonction}',
        '${employe.salary}'
        );`).catch((err) => {
        console.log(err);
    });
    res.json(query);
}));
router2.delete('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse DELETE Request');
    let id = req.params.id;
    console.log(req.params.id);
    const query = yield connectionConfig.query(`DELETE FROM Employe WHERE id = '${id}';`).catch((err) => {
        console.log(err);
    });
    res.json(query);
}));
/****************************************************************************************************************/
/****************************************************PETS********************************************************/
router3.post('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse POST Request');
    let pet = req.body;
    console.log(pet);
    const query = yield connectionConfig.query(`INSERT INTO Pet VALUES (
    '${pet.id}', 
    '${pet.id_clinic}', 
    '${pet.name}', 
    '${pet.type}', 
    '${pet.specie}',
    '${pet.size}',
    '${pet.weight}',
    '${pet.description}', 
    '${pet.birthdate}', 
    '${pet.inscriptiondate}',
    '${pet.state}',
    '${pet.ownerid}'
    );`).catch((err) => {
        console.log(err);
    });
    res.json(query);
}));
router3.get('/pets', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    const clients = yield connectionConfig.query("SELECT * FROM Pet ORDER BY id;");
    let result = clients.rows.map((pet) => ({
        id: pet.id,
        id_clinic: pet.id_clinic,
        name: pet.name,
        type: pet.type,
        specie: pet.specie,
        size: pet.size,
        weight: pet.weight,
        description: pet.description,
        state: pet.state,
        ownerid: pet.ownerid,
        birthdate: pet.birthdate,
        inscriptiondate: pet.inscriptiondate,
    }));
    //console.log(result);
    res.json(result);
}));
router3.get('/:name', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    let name = req.params.name;
    const clients = yield connectionConfig.query(`SELECT * FROM Pet WHERE name LIKE '%${name}%'ORDER BY id;`);
    let result = clients.rows.map((pet) => ({
        id: pet.id,
        id_clinic: pet.id_clinic,
        name: pet.name,
        type: pet.type,
        specie: pet.specie,
        size: pet.size,
        weight: pet.weight,
        description: pet.description,
        state: pet.state,
        ownerid: pet.ownerid,
        birthdate: pet.birthdate,
        inscriptiondate: pet.inscriptiondate,
    }));
    //console.log(result);
    res.json(result);
}));
router3.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    let id = req.params.id;
    const clients = yield connectionConfig.query(`SELECT * FROM Pet WHERE OwnerId = '${id}' ORDER BY id;`);
    let result = clients.rows.map((pet) => ({
        id: pet.id,
        id_clinic: pet.id_clinic,
        name: pet.name,
        type: pet.type,
        specie: pet.specie,
        size: pet.size,
        weight: pet.weight,
        description: pet.description,
        state: pet.state,
        ownerid: pet.ownerid,
        birthdate: pet.birthdate,
        inscriptiondate: pet.inscriptiondate,
    }));
    //console.log(result);
    res.json(result);
}));
router3.delete('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse DELETE Request');
    let id = req.params.id;
    console.log(req.params.id);
    const query = yield connectionConfig.query(`DELETE FROM Pet WHERE id = '${id}';`).catch((err) => {
        console.log(err);
    });
    res.json(query);
}));
/****************************************************************************************************************/
/****************************************************OWNERS********************************************************/
router4.post('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse POST Request');
    let owner = req.body;
    console.log(owner);
    const query = yield connectionConfig.query(`INSERT INTO Owner VALUES (
    '${owner.id}', 
    '${owner.id_clinic}', 
    '${owner.name}', 
    '${owner.adress}', 
    '${owner.phonenb}'
    );`).catch((err) => {
        console.log(err);
    });
    res.json(query);
}));
router4.get('/owners', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    const clients = yield connectionConfig.query("SELECT * FROM Owner ORDER BY id;");
    let result = clients.rows.map((owner) => ({
        id: owner.id,
        id_clinic: owner.id_clinic,
        name: owner.name,
        adress: owner.adress,
        phonenb: owner.phonenb,
    }));
    //console.log(result);
    res.json(result);
}));
router4.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    let id = req.params.id;
    const clients = yield connectionConfig.query(`SELECT * FROM Owner WHERE id_clinic = '${id}' ORDER BY id;`);
    let result = clients.rows.map((owner) => ({
        id: owner.id,
        id_clinic: owner.id_clinic,
        name: owner.name,
        adress: owner.adress,
        phonenb: owner.phonenb,
    }));
    //console.log(result);
    res.json(result);
}));
router4.delete('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse DELETE Request');
    let id = req.params.id;
    console.log(req.params.id);
    const query = yield connectionConfig.query(`DELETE FROM Owner WHERE id = '${id}';`).catch((err) => {
        console.log(err);
    });
    res.json(query);
}));
/****************************************************************************************************************/
/**************************************************SALARY****************************************************/
router5.get('/clinicsSalarys', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    let query = 'SELECT c.Id , SUM(e.salary) AS salary FROM clinic c LEFT JOIN Employe e ON ( e.id = (SELECT id_employe FROM ClinicEmploye WHERE id_employe = e.id AND c.id = id_clinic) ) GROUP BY c.Id;';
    const clients = yield connectionConfig.query(query);
    let result = clients.rows.map((salary) => ({
        id: salary.id,
        salary: salary.salary,
    }));
    res.json(result);
}));
/****************************************************************************************************************/
router6.get('/occurences', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    const clients = yield connectionConfig.query("SELECT name, Count(*) as occurence FROM Pet GROUP BY name;");
    let result = clients.rows.map((occurence) => ({
        name: occurence.name,
        occurence: occurence.occurence,
    }));
    res.json(result);
}));
/****************************************************************************************************************/
/****************************************************************************************************************/
router7.get('/traitements', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    const clients = yield connectionConfig.query("SELECT p.ExamID, e.Date, e.petId, e.VeterinaryId, p.TraitementId, p.billid, p.quantity, p.StartDate , p.EndDate FROM Prescription p, Exam e WHERE e.id = p.ExamID;");
    let result = clients.rows.map((traitement) => ({
        examid: traitement.examid,
        date: traitement.date,
        petid: traitement.petid,
        veterinaryid: traitement.veterinaryid,
        traitementid: traitement.traitementid,
        billid: traitement.billid,
        quantity: traitement.quantity,
        startdate: traitement.startdate,
        enddate: traitement.enddate
    }));
    res.json(result);
}));
router7.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    let id = req.params.id;
    const clients = yield connectionConfig.query(`SELECT p.ExamID, e.Date, e.petId, e.VeterinaryId, p.TraitementId, p.billid, p.quantity, p.StartDate , p.EndDate FROM Prescription p, Exam e WHERE e.id = p.ExamID AND e.petId = '${id}' ;`);
    let result = clients.rows.map((traitement) => ({
        examid: traitement.examid,
        date: traitement.date,
        petid: traitement.petid,
        veterinaryid: traitement.veterinaryid,
        traitementid: traitement.traitementid,
        billid: traitement.billid,
        quantity: traitement.quantity,
        startdate: traitement.startdate,
        enddate: traitement.enddate
    }));
    res.json(result);
}));
/****************************************************************************************************************/
/*************************************************BILL**********************************************************/
router8.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    let id = req.params.id;
    const clients = yield connectionConfig.query(`SELECT * FROM Bill WHERE id = '${id}' ;`);
    let result = clients.rows.map((bill) => ({
        id: bill.id,
        ownerid: bill.ownerid,
        petid: bill.petid,
        veterinaryid: bill.veterinaryid,
        date: bill.date,
        paymentmethod: bill.paymentmethod,
        paymentaccepted: bill.paymentaccepted,
        total: bill.total,
    }));
    res.json(result);
}));
router8.post('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse POST Request');
    let bill = req.body;
    console.log(bill);
    const query = yield connectionConfig.query(`INSERT INTO Bill VALUES (
    '${bill.id}', 
    '${bill.ownerid}', 
    '${bill.petid}', 
    '${bill.veterinaryid}',
    '${bill.date}', 
    '${bill.paymentmethod}',
    '${bill.paymentaccepted}', 
    '${bill.total}'
    );`).catch((err) => {
        console.log(err);
    });
    res.json(query);
}));
/*************************************************BILL**********************************************************/
/*************************************************TREATEMENT DETAILS**********************************************************/
router9.get('/:id', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('server anylyse GET Request');
    let id = req.params.id;
    const clients = yield connectionConfig.query(`SELECT * FROM Treatment WHERE id = '${id}' ;`);
    let result = clients.rows.map((detail) => ({
        id: detail.id,
        description: detail.description,
        price: detail.price,
    }));
    res.json(result);
}));
/*************************************************TREATEMENT DETAILS**********************************************************/
app.listen(port, () => {
    console.log('serveur running on port ' + port);
});
//# sourceMappingURL=app.js.map